from distutils.core import setup

import py2exe
import os

import matplotlib       #Import then use get_py2exe_datafiles() to collect numpy datafiles.
matplotlib.use('wxagg') #Specify matplotlib backend. tkagg must still be included else error is thrown. 

#import zmq

# libzmq.dll is in same directory as zmq's __init__.py
#os.environ["PATH"] = os.environ["PATH"] + os.path.pathsep + os.path.split(zmq.__file__)[0]

includes = ["matplotlib.figure", "pylab", 'matplotlib.backends.backend_wx']
excludes = ['_gtkagg', '_tkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
            'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
            'Tkconstants', 'Tkinter']
packages = []
dll_excludes = ['libgdk-win32-2.0-0.dll', 'libgobject-2.0-0.dll', 'tcl84.dll','tk84.dll']

icon_folder = "icons" # where to keep the icons
icon_files = ["icons\\icon.ico"] # List of icons
exe_ico = "icons\\icon.ico"  # icon for the executable file
script = "FITSFileViewer.py" # Main script file

dist_dir = "distribute"  # Directory name for the final exe file

data_files = [	(icon_folder,icon_files)]
# Extend the tuple list because matplotlib returns a tuple list.      
data_files.extend(matplotlib.get_py2exe_datafiles())  #Matplotlib - pulls it's own files

setup(name='FITSFileViewer', options = {"py2exe": {"compressed": 2, "optimize": 2,"includes": includes,"excludes": excludes,"packages": packages,
					"dll_excludes": dll_excludes,"bundle_files": 3,"dist_dir": dist_dir,"xref": False,
					"skip_archive": False,"ascii": False,"custom_boot_script": '',}},
					windows=[
									{
										"script": script,
										"icon_resources": [(50, exe_ico),(51, exe_ico),(52, exe_ico)]
									}
								],
					data_files=data_files)
