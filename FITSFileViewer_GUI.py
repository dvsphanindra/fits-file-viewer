# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun  5 2014)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

ID_OPEN_FILE = 1000
ID_QUIT = 1001
ID_ACCENT = 1002
ID_AUTUMN = 1003
ID_BONE = 1004
ID_COOL = 1005
ID_COOLWARM = 1006
ID_COPPER = 1007
ID_CUBE_HELIX = 1008
ID_GIST_GREY = 1009
ID_GIST_HEAT = 1010
ID_GRAYSCALE = 1011
ID_HOT = 1012
ID_JET = 1013
ID_OCEAN = 1014
ID_RAINBOW = 1015
ID_SPRING = 1016
ID_SUMMER = 1017
ID_TERRAIN = 1018
ID_WINTER = 1019
ID_ABOUT = 1020

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"FITS File Viewer", pos = wx.DefaultPosition, size = wx.Size( 831,748 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.MAXIMIZE_BOX|wx.MINIMIZE_BOX|wx.RESIZE_BORDER|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 224, 224, 224 ) )
		
		self.menubar = wx.MenuBar( 0 )
		self.file = wx.Menu()
		self.openFile = wx.MenuItem( self.file, ID_OPEN_FILE, u"&Open File..."+ u"\t" + u"CTRL+O", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.openFile )
		
		self.file.AppendSeparator()
		
		self.quit = wx.MenuItem( self.file, ID_QUIT, u"&Quit"+ u"\t" + u"CTRL+Q", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.quit )
		
		self.menubar.Append( self.file, u"&File" ) 
		
		self.colorMap = wx.Menu()
		self.accent = wx.MenuItem( self.colorMap, ID_ACCENT, u"Accent", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.accent )
		
		self.autumn = wx.MenuItem( self.colorMap, ID_AUTUMN, u"Autumn", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.autumn )
		
		self.bone = wx.MenuItem( self.colorMap, ID_BONE, u"Bone", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.bone )
		
		self.cool = wx.MenuItem( self.colorMap, ID_COOL, u"Cool", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.cool )
		
		self.coolwarm = wx.MenuItem( self.colorMap, ID_COOLWARM, u"Coolwarm", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.coolwarm )
		
		self.copper = wx.MenuItem( self.colorMap, ID_COPPER, u"Copper", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.copper )
		
		self.cubeHelix = wx.MenuItem( self.colorMap, ID_CUBE_HELIX, u"Cube Helix", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.cubeHelix )
		
		self.gistGrey = wx.MenuItem( self.colorMap, ID_GIST_GREY, u"Gist Grey", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.gistGrey )
		
		self.gistHeat = wx.MenuItem( self.colorMap, ID_GIST_HEAT, u"Gist Heat", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.gistHeat )
		
		self.grayscale = wx.MenuItem( self.colorMap, ID_GRAYSCALE, u"GrayScale", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.grayscale )
		self.grayscale.Check( True )
		
		self.hot = wx.MenuItem( self.colorMap, ID_HOT, u"Hot", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.hot )
		
		self.jet = wx.MenuItem( self.colorMap, ID_JET, u"Jet", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.jet )
		
		self.ocean = wx.MenuItem( self.colorMap, ID_OCEAN, u"Ocean", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.ocean )
		
		self.rainbow = wx.MenuItem( self.colorMap, ID_RAINBOW, u"Rainbow", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.rainbow )
		
		self.spring = wx.MenuItem( self.colorMap, ID_SPRING, u"Spring", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.spring )
		
		self.summer = wx.MenuItem( self.colorMap, ID_SUMMER, u"Summer", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.summer )
		
		self.terrain = wx.MenuItem( self.colorMap, ID_TERRAIN, u"Terrain", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.terrain )
		
		self.winter = wx.MenuItem( self.colorMap, ID_WINTER, u"Winter", wx.EmptyString, wx.ITEM_RADIO )
		self.colorMap.AppendItem( self.winter )
		
		self.menubar.Append( self.colorMap, u"&Color Map" ) 
		
		self.help = wx.Menu()
		self.about = wx.MenuItem( self.help, ID_ABOUT, u"&About", wx.EmptyString, wx.ITEM_NORMAL )
		self.help.AppendItem( self.about )
		
		self.menubar.Append( self.help, u"&Help" ) 
		
		self.SetMenuBar( self.menubar )
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_splitter1 = wx.SplitterWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D|wx.SP_LIVE_UPDATE|wx.STATIC_BORDER )
		self.m_splitter1.SetSashGravity( 0.1 )
		self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )
		self.m_splitter1.SetMinimumPaneSize( 100 )
		
		self.m_splitter1.SetBackgroundColour( wx.Colour( 224, 224, 224 ) )
		
		self.m_panel4 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer7 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText3 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"FITS Images:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )
		bSizer7.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		self.treeCtrl_FITSinfo = wx.TreeCtrl( self.m_panel4, wx.ID_ANY, wx.DefaultPosition, wx.Size( 170,-1 ), wx.TR_DEFAULT_STYLE|wx.TR_HIDE_ROOT )
		self.treeCtrl_FITSinfo.SetBackgroundColour( wx.Colour( 224, 224, 224 ) )
		
		bSizer7.Add( self.treeCtrl_FITSinfo, 5, wx.ALL|wx.EXPAND, 2 )
		
		
		bSizer7.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		self.m_panel4.SetSizer( bSizer7 )
		self.m_panel4.Layout()
		bSizer7.Fit( self.m_panel4 )
		self.m_panel5 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		fgSizer1 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer1.AddGrowableCol( 0 )
		fgSizer1.AddGrowableRow( 1 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_ALL )
		
		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticText_filename = wx.StaticText( self.m_panel5, wx.ID_ANY, u"FITS File:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_filename.Wrap( -1 )
		self.staticText_filename.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer4.Add( self.staticText_filename, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.staticText_colormap = wx.StaticText( self.m_panel5, wx.ID_ANY, u"Colormap:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_colormap.Wrap( -1 )
		self.staticText_colormap.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer4.Add( self.staticText_colormap, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		fgSizer1.Add( bSizer4, 1, wx.EXPAND, 5 )
		
		
		fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.panel_FITSimage = wx.Panel( self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		fgSizer1.Add( self.panel_FITSimage, 1, wx.ALL|wx.EXPAND, 1 )
		
		self.panel_verticalCut = wx.Panel( self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		fgSizer1.Add( self.panel_verticalCut, 1, wx.ALL|wx.EXPAND, 1 )
		
		self.panel_horizontalCut = wx.Panel( self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		fgSizer1.Add( self.panel_horizontalCut, 1, wx.ALL|wx.EXPAND, 1 )
		
		bSizer51 = wx.BoxSizer( wx.VERTICAL )
		
		self.staticText_VerticalYValue = wx.StaticText( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_VerticalYValue.Wrap( -1 )
		bSizer51.Add( self.staticText_VerticalYValue, 1, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 2 )
		
		self.staticText_HorizontalXValue = wx.StaticText( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_HorizontalXValue.Wrap( -1 )
		bSizer51.Add( self.staticText_HorizontalXValue, 1, wx.ALL|wx.EXPAND, 2 )
		
		self.staticText_DataValue = wx.StaticText( self.m_panel5, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_DataValue.Wrap( -1 )
		bSizer51.Add( self.staticText_DataValue, 0, wx.ALL|wx.EXPAND, 2 )
		
		self.button_OpenFITSFile = wx.Button( self.m_panel5, wx.ID_ANY, u"Open FITS Files...", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer51.Add( self.button_OpenFITSFile, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		fgSizer1.Add( bSizer51, 1, wx.EXPAND, 5 )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.panel_colorMap = wx.Panel( self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		bSizer6.Add( self.panel_colorMap, 1, wx.EXPAND |wx.ALL, 2 )
		
		
		fgSizer1.Add( bSizer6, 1, wx.EXPAND, 5 )
		
		bSizer71 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText7 = wx.StaticText( self.m_panel5, wx.ID_ANY, u" ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		bSizer71.Add( self.m_staticText7, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText71 = wx.StaticText( self.m_panel5, wx.ID_ANY, u" ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText71.Wrap( -1 )
		bSizer71.Add( self.m_staticText71, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		fgSizer1.Add( bSizer71, 1, wx.EXPAND, 5 )
		
		
		self.m_panel5.SetSizer( fgSizer1 )
		self.m_panel5.Layout()
		fgSizer1.Fit( self.m_panel5 )
		self.m_splitter1.SplitVertically( self.m_panel4, self.m_panel5, 180 )
		bSizer1.Add( self.m_splitter1, 1, wx.EXPAND, 2 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		self.statusBar = self.CreateStatusBar( 2, wx.ST_SIZEGRIP, wx.ID_ANY )
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.onQuit )
		self.Bind( wx.EVT_MENU, self.button_OpenFITSFile_Click, id = self.openFile.GetId() )
		self.Bind( wx.EVT_MENU, self.onQuit, id = self.quit.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.accent.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.autumn.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.bone.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.cool.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.coolwarm.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.copper.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.cubeHelix.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.gistGrey.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.gistHeat.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.grayscale.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.hot.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.jet.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.ocean.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.rainbow.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.spring.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.summer.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.terrain.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_colormap_selection, id = self.winter.GetId() )
		self.Bind( wx.EVT_MENU, self.On_About_Select, id = self.about.GetId() )
		self.treeCtrl_FITSinfo.Bind( wx.EVT_LEFT_DCLICK, self.treeCtrl_FITSinfo_LeftDClick )
		self.treeCtrl_FITSinfo.Bind( wx.EVT_TREE_ITEM_RIGHT_CLICK, self.treeCtrl_FITSinfo_RightClick )
		self.button_OpenFITSFile.Bind( wx.EVT_BUTTON, self.button_OpenFITSFile_Click )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def onQuit( self, event ):
		event.Skip()
	
	def button_OpenFITSFile_Click( self, event ):
		event.Skip()
	
	
	def menu_colormap_selection( self, event ):
		event.Skip()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	def On_About_Select( self, event ):
		event.Skip()
	
	def treeCtrl_FITSinfo_LeftDClick( self, event ):
		event.Skip()
	
	def treeCtrl_FITSinfo_RightClick( self, event ):
		event.Skip()
	
	
	def m_splitter1OnIdle( self, event ):
		self.m_splitter1.SetSashPosition( 180 )
		self.m_splitter1.Unbind( wx.EVT_IDLE )
	

