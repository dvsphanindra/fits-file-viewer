#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 06 15:13:05 2014

@author: D.V.S. Phanindra
"""
import FITSFileViewer_GUI

import wx
import wx.html

import os
import sys
import platform

from astropy.io import fits as pyfits
import matplotlib.cm as cm

import numpy as np
from matplotlib.pyplot import figure as Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigCanvas

from matplotlib.patches import Rectangle

LEFT_KEY = 314
UP_KEY = 315
RIGHT_KEY = 316
DOWN_KEY = 317

aboutText = """<p><font face="Bookman Old Style" color = 	#5582C4 size = 15>FITS File Viewer Version 1.0</font></p> <p>Developed by <b>D.V.S. Phanindra</b>.</p>
<p>Created using <b>wxPython:</b> 3.0.1.1, <b> and <b>Python:</b> 2.7.9.</p>
<p>Installed version: <b>wxPython:</b> %(wxpy)s and <b>Python:</b> %(python)s.</p></p>"""

###############################################################################
class HtmlWindow(wx.html.HtmlWindow):
	def __init__(self, parent, id, size=(600,400)):
		wx.html.HtmlWindow.__init__(self,parent, id, size=size)
		if "gtk2" in wx.PlatformInfo:
			self.SetStandardFonts()
	
	#---------------------------------------------------------------------------
	def OnLinkClicked(self, link):
		wx.LaunchDefaultBrowser(link.GetHref())

###############################################################################
class AboutBox(wx.Dialog):
	def __init__(self):
		wx.Dialog.__init__(self, None, -1, "Testing wx and matplotlib",
			style=wx.DEFAULT_DIALOG_STYLE|wx.THICK_FRAME|wx.RESIZE_BORDER|wx.TAB_TRAVERSAL)
		hwin = HtmlWindow(self, -1, size=(450,200))
		vers = {}
		vers["python"] = sys.version.split()[0]
		vers["wxpy"] = wx.VERSION_STRING
		hwin.SetPage(aboutText % vers)
		btn = hwin.FindWindowById(wx.ID_OK)
		irep = hwin.GetInternalRepresentation()
		hwin.SetSize((irep.GetWidth()+25, irep.GetHeight()+10))
		self.SetClientSize(hwin.GetSize())
		self.CentreOnParent(wx.BOTH)
		self.SetFocus()
		
###############################################################################
class FileDrop(wx.FileDropTarget):
	def __init__(self, dropTargetWindow):
		wx.FileDropTarget.__init__(self)
		self.dropTargetWindow = dropTargetWindow
		
	#---------------------------------------------------------------------------
	def OnDropFiles(self, x, y, files):
		for filename in files[0:-1]:
			self.dropTargetWindow.openFITSFile(filename, displayImage=False)
		self.dropTargetWindow.openFITSFile(files[-1], displayImage=True)
		
###############################################################################
class FITSFileViewer(FITSFileViewer_GUI.MainFrame):
	""" Subclass the form created by wxFormBuilder. """
	
	def __init__(self, parent):
		# Ensure that the parent is initialized
		FITSFileViewer_GUI.MainFrame.__init__(self, parent)
		
		
		self.OS = platform.system()
		
		self.image = None
		self.image_original = None
		self.image_x = None
		self.image_y = None
		self.image_xLim = (0,0)
		self.image_yLim = (0,0)
		self.image_hLine = None
		self.image_vLine = None
		self.hCutLine = None
		self.vCutLine = None
		self.yValue = 0
		self.xValue = 0
		self.drawRect = False
		self.selectionPatch = None
		self.treeRoot = self.treeCtrl_FITSinfo.AddRoot("Properties")
		self.tree_Items = []
		
		self.tree_menu_titles = [ "Open","Delete" ]
		self.tree_menu_title_by_id = {}
		
		self.colormapList = {	"Autumn":cm.autumn, "Accent":cm.Accent, "Bone":cm.bone, "Cool":cm.cool, "Coolwarm":cm.coolwarm,
										"Copper": cm.copper, "Cube Helix":cm.cubehelix, "Gist Grey":cm.gist_gray, "Gist Heat":cm.gist_heat,
										"GrayScale":cm.gray, "Hot":cm.hot, "Jet":cm.jet, "Ocean":cm.ocean, "Rainbow":cm.rainbow,
										"Spring":cm.spring, "Summer":cm.summer, "Terrain":cm.terrain, "Winter":cm.winter }

		self.imageColorMap = self.colormapList["GrayScale"]
		
		for menu_title in self.tree_menu_titles:
			self.tree_menu_title_by_id[ wx.NewId() ] = menu_title
			
		### 2. Launcher creates wxMenu. ###
		self.tree_contextMenu = wx.Menu()
		for (id,title) in self.tree_menu_title_by_id.items():
			### 3. Launcher packs menu with Append. ###
			self.tree_contextMenu.Append( id, title )
			### 4. Launcher registers menu handlers with EVT_MENU, on the menu. ###
			wx.EVT_MENU( self.tree_contextMenu, id, self.contextMenuSelected )
		
		self.SetIcon(wx.Icon(os.path.join('icons','icon.ico'), wx.BITMAP_TYPE_ICO, 32, 32))
		
		# Create an accelerator table
		CTRL_LEFTKeyPress = wx.NewId()
		CTRL_RIGHTKeyPress = wx.NewId()
		CTRL_UPKeyPress = wx.NewId()
		CTRL_DOWNKeyPress = wx.NewId()
		self.Bind(wx.EVT_MENU, self.CTRL_LEFTKeyPressed, id = CTRL_LEFTKeyPress)
		self.Bind(wx.EVT_MENU, self.CTRL_RIGHTKeyPressed, id = CTRL_RIGHTKeyPress)
		self.Bind(wx.EVT_MENU, self.CTRL_UPKeyPressed, id = CTRL_UPKeyPress)
		self.Bind(wx.EVT_MENU, self.CTRL_DOWNKeyPressed, id = CTRL_DOWNKeyPress)
 
		self.accel_tbl = wx.AcceleratorTable([	(wx.ACCEL_CTRL, LEFT_KEY, CTRL_LEFTKeyPress),
																(wx.ACCEL_CTRL, RIGHT_KEY, CTRL_RIGHTKeyPress),
																(wx.ACCEL_CTRL, UP_KEY, CTRL_UPKeyPress),
																(wx.ACCEL_CTRL, DOWN_KEY, CTRL_DOWNKeyPress)])
		self.SetAcceleratorTable(self.accel_tbl)
		
		dt = FileDrop(self)
		self.SetDropTarget(dt)
		
		# Create a matplotlib Figure
		self.figure1 = Figure(figsize=(4, 6.45),tight_layout=True)
		self.axes1 = self.figure1.add_subplot(111,axisbg='Gainsboro',alpha='0.5')
#		self.axes1.xaxis.tick_top()
#		self.axes1.invert_yaxis()
		# And add it to the appropriate panel
		self.canvas1 = FigCanvas(self.panel_FITSimage, wx.ID_ANY, self.figure1)
		# To adjust to fit the panel
		sizer1 = wx.BoxSizer(wx.VERTICAL)
		sizer1.Add(self.canvas1, 1, wx.EXPAND)
		self.panel_FITSimage.SetSizer(sizer1)
		self.panel_FITSimage.Fit()
		self.figure1.canvas.mpl_connect("button_press_event", self.axes1_onClick)
		self.figure1.canvas.mpl_connect("motion_notify_event", self.axes1_mouseMotion)
		self.figure1.canvas.mpl_connect('button_release_event', self.axes1_onClickRelease)
		
		# Create a matplotlib Figure
		self.figure2 = Figure(figsize=(4, 1),tight_layout=True)
		self.axes2 = self.figure2.add_subplot(111,axisbg='Gainsboro',alpha='0.5')
		self.axes2.yaxis.set_ticks(np.linspace(0,1,6))
		self.axes2.set_yticklabels(["", "", "", "", "", "1"])
		# And add it to the appropriate panel
		self.canvas2 = FigCanvas(self.panel_horizontalCut, wx.ID_ANY, self.figure2)
		# To adjust to fit the panel
		sizer2 = wx.BoxSizer(wx.VERTICAL)
		sizer2.Add(self.canvas2, 1, wx.EXPAND)
		self.panel_horizontalCut.SetSizer(sizer2)
		self.panel_horizontalCut.Fit()
		self.figure2.canvas.mpl_connect("motion_notify_event", self.axes2_mouseMotion)
		
		# Create a matplotlib Figure
		self.figure3 = Figure(figsize=(1, 6.45),tight_layout=True)
		self.axes3 = self.figure3.add_subplot(111,axisbg='Gainsboro',alpha='0.5')
		self.axes3.xaxis.set_ticks(np.linspace(0,1,6))
		self.axes3.set_xticklabels(["0", "", "", "", "", ""], rotation = "vertical")
		self.axes3.invert_xaxis()
		# And add it to the appropriate panel
		self.canvas3 = FigCanvas(self.panel_verticalCut, wx.ID_ANY, self.figure3)
		# To adjust to fit the panel
		sizer3 = wx.BoxSizer(wx.VERTICAL)
		sizer3.Add(self.canvas3, 1, wx.EXPAND)
		self.panel_verticalCut.SetSizer(sizer3)
		self.panel_verticalCut.Fit()
		self.figure3.canvas.mpl_connect("motion_notify_event", self.axes3_mouseMotion)
		
		# Create a matplotlib Figure
		self.figure4 = Figure(figsize=(4, 1),tight_layout=True)
		self.axes4 = self.figure4.add_subplot(111)
		self.axes4.get_xaxis().set_ticks([])
		self.axes4.get_yaxis().set_ticks([])
		self.colormap_display_object=np.outer(np.arange(0,1,0.01),np.ones((10,1))).T
		self.axes4.imshow(self.colormap_display_object,aspect='auto',cmap=cm.get_cmap(self.imageColorMap),origin="lower")
		# And add it to the appropriate panel
		self.canvas4 = FigCanvas(self.panel_colorMap, wx.ID_ANY, self.figure4)
		# To adjust to fit the panel
		sizer4 = wx.BoxSizer(wx.VERTICAL)
		sizer4.Add(self.canvas4, 1, wx.EXPAND)
		self.panel_colorMap.SetSizer(sizer4)
		self.panel_colorMap.Fit()
	
	#---------------------------------------------------------------------------
	def button_OpenFITSFile_Click(self,event):
		dialog = wx.FileDialog(self, "Open FITS file", "","", "FITS Files (*.fits)|*.fits|" + "All files (*.*)|*.*", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST|wx.MULTIPLE)
		dialog.ShowModal()
		path, name = os.path.split(dialog.GetPath())
		FITSfiles = dialog.GetFilenames()
		dialog.Destroy()
		
		if FITSfiles != []:
			for FITSfile in FITSfiles[0:-1]:
				self.openFITSFile(os.path.join(path, FITSfile), displayImage = False)
				
			self.openFITSFile(os.path.join(path, FITSfiles[-1]), displayImage = True)
			
	#---------------------------------------------------------------------------
	def openFITSFile(self, FITSfile, displayImage = True):
		self.FITSheader = pyfits.getheader(FITSfile)
		keys = self.FITSheader.keys()
		
		filepath, filename  = os.path.split(FITSfile)
		
		self.tree_Items.append(self.treeCtrl_FITSinfo.AppendItem(self.treeRoot,filename)) # Append the file to the tree
		self.treeCtrl_FITSinfo.AppendItem(self.tree_Items[-1], "Location: " + filepath)  # Add data to the last (latest) item in the tree
		for key in keys:
			self.treeCtrl_FITSinfo.AppendItem(self.tree_Items[-1], key + ": " + str(self.FITSheader[key])) # Add data to the last (latest) item in the tree
		
		if displayImage:
			self.displayFITSImage(FITSfile)
	
	#---------------------------------------------------------------------------
	def displayFITSImage(self, FITSfile):
		image = pyfits.getdata(FITSfile,0)
		
		if self.FITSheader["NAXIS"] == 2:
			self.image = np.array(image)
		else:
			self.image = np.array(image[0])
		
		# Initialize all image related variables
		self.image_original = None
		self.image_x = None
		self.image_y = None
		self.image_xLim = (0,0)
		self.image_yLim = (0,0)
		self.image_hLine = None
		self.image_vLine = None
		self.hCutLine = None
		self.vCutLine = None
		self.yValue = 0
		self.xValue = 0
		
		self.image_original = self.image # TODO deep copy may be required
		
		self.staticText_filename.SetLabel("FITS File: " + FITSfile.split('\\')[-1])
		self.staticText_colormap.SetLabel("Colormap: " + "GrayScale")
		self.SetTitle("FITS File Viewer: " + FITSfile.split('\\')[-1])
		
		self.image_xLim = (0,self.FITSheader["NAXIS1"])
		self.image_yLim = (0,self.FITSheader["NAXIS2"])
		
		self.axes2.clear()
		self.axes2.yaxis.set_ticks(np.linspace(0,1,6))
		self.axes2.set_yticklabels(["", "", "", "", "", "1"])
		self.canvas2.draw()
		self.axes3.clear()
		self.axes3.xaxis.set_ticks(np.linspace(0,1,6))
		self.axes3.set_xticklabels(["0", "", "", "", "", ""], rotation = "vertical")
		self.canvas3.draw()
		
		self.axes1_drawplot()
	
	#---------------------------------------------------------------------------
	def axes1_drawplot(self):
		wx.BusyInfo("Updating Image...")
		self.axes1.clear()
		self.axes1.imshow(self.image, self.imageColorMap)
		self.axes1.set_xlim(self.image_xLim)
		self.axes1.set_ylim(self.image_yLim)
		
		self.canvas1.draw()
		
	#---------------------------------------------------------------------------
	def updatePlots(self):
		if self.image_hLine != None:
			self.image_hLine.remove()
			self.image_vLine.remove()
			self.hCutLine.remove()
			self.vCutLine.remove()
		
		self.image_x = self.image[self.yValue,:]
		self.image_y = self.image[:,self.xValue]
		self.image_hLine = self.axes1.axhline(self.yValue, color = 'red', linestyle = "-.")
		self.image_vLine = self.axes1.axvline(self.xValue, color = 'red', linestyle = "-.")
		self.canvas1.draw()
		
		min_y = min(self.image_x)
		max_y = max(self.image_x)
		self.axes2.clear()
		self.axes2.plot(self.image_x, 'black')
		self.axes2.set_xlim(self.image_xLim)
		self.axes2.set_ylim(min_y, max_y)
		self.axes2.yaxis.set_ticks(np.linspace(min_y,max_y,6))
		self.axes2.set_yticklabels([min_y, "", "", "", "", max_y])
		self.hCutLine = self.axes2.axvline(self.xValue, color = 'red')
		self.canvas2.draw()
		
		dummy_data = range(self.image_xLim[1]-self.image_xLim[0])
		min_x = min(self.image_y)
		max_x = max(self.image_y)
#		print "Shapes: ",
#		print np.shape(self.image_y), np.shape(dummy_data)
		self.axes3.clear()
		self.axes3.plot(self.image_y, dummy_data, 'black')
		self.axes3.set_xlim(min_x,max_x)
		self.axes3.set_ylim(self.image_yLim)
		self.axes3.xaxis.set_ticks(np.linspace(min_x,max_x,6))
		self.axes3.set_xticklabels([min_x, "", "", "", "", max_x], rotation = "vertical")
		self.axes3.invert_xaxis()
		self.vCutLine = self.axes3.axhline(self.yValue, color = 'red')
		self.canvas3.draw()
		
		self.staticText_VerticalYValue.SetLabel("Y = " + str(self.yValue))
		self.staticText_DataValue.SetLabel("Data = " + str(self.image_x[self.xValue]))
		self.staticText_HorizontalXValue.SetLabel("X = " + str(self.xValue))
	
	
	#---------------------------------------------------------------------------
	def axes1_onClick(self,event):
		if event.button == 1:
			
			if self.image != None:
				x = 0
				y = 0
				if event.xdata != None:
					x = int(event.xdata)
					y = int(event.ydata)
					
					self.drawRect = True
					self.drawRect_x0 = x
					self.drawRect_y0 = y
				
				self.xValue = x
				self.yValue = y
				
				self.updatePlots()
	
	#---------------------------------------------------------------------------
	def axes1_onClickRelease(self, event):
		#print "release"
		if self.drawRect:
			x = int(event.xdata)
			y = int(event.ydata)
			
			if self.drawRect_x0 < x:
				y0 = self.drawRect_x0
				y1 = x
			else:
				y0 = x
				y1 = self.drawRect_x0
				
			if self.drawRect_y0 < y:
				x0 = self.drawRect_y0 # TODO to correct this
				x1 = y
			else:
				x0 = y
				x1 = self.drawRect_y0
			
#			print x0, x1, y0, y1
#			print "Before: ",
#			print np.shape(self.image)
			if (x1 - x0) * (y1 - y0) > 40000:
				self.image = self.image[x0:x1, y0:y1]
				self.image_xLim = (0,x1-x0)
				self.image_yLim = (0,y1-y0)
				
				self.image_x = None
				self.image_y = None
				self.image_hLine = None
				self.image_vLine = None
				self.hCutLine = None
				self.vCutLine = None
				
#				print "After: ",
#				print np.shape(self.image)
				self.drawRect = False
				self.selectionPatch = None
				self.axes1.clear()
				self.axes1.imshow(self.image, self.imageColorMap)
				self.axes1.set_xlim(self.image_yLim)
				self.axes1.set_ylim(self.image_xLim)
				self.canvas1.draw()
				
			self.drawRect = False
			self.selectionPatch = None
	
	
	#---------------------------------------------------------------------------
	def axes1_mouseMotion(self,event):
		x = 0
		y = 0
		if event.xdata != None:
			x = int(event.xdata)
			y = int(event.ydata)
			
			if self.drawRect:
				if self.selectionPatch == None:
					self.selectionPatch = self.axes1.add_patch(Rectangle((self.drawRect_x0, self.drawRect_y0), (x-self.drawRect_x0), (y-self.drawRect_y0), facecolor=self.imageColorMap(range(256))[-1], alpha=0.5, joinstyle = 'miter', ls='dashdot'))
				else:
					self.selectionPatch.set_width(x-self.drawRect_x0)
					self.selectionPatch.set_height(y-self.drawRect_y0)
				self.canvas1.draw()
			
		if self.image == None or x == 0 or y == 0:
			self.SetStatusText("X = " + str(x) + ", Y = " + str(y) + ", Data = 0.0", 0)
		else:
			self.SetStatusText("X = " + str(x) + ", Y = " + str(y) + ", Data = " + str(self.image[x][self.image_yLim[1] - y]), 0)
	
	#---------------------------------------------------------------------------
	def axes2_mouseMotion(self,event):
		x = 0
		y = 0
		if event.xdata != None:
			x = int(event.xdata)
			y = int(event.ydata)
		if self.image_x == None or x == 0 or y == 0:
			self.SetStatusText("X = " + str(x) + ", Data = 0.0", 0)
		else:
			self.SetStatusText("X = " + str(x) + ", Data = " + str(self.image_x[x]), 0)
	
	#---------------------------------------------------------------------------
	def axes3_mouseMotion(self,event):
		x = 0
		y = 0
		if event.xdata != None:
			x = int(event.xdata)
			y = int(event.ydata)
		if self.image_y == None or x == 0 or y == 0:
			self.SetStatusText("Y = " + str(y) + ", Data = 0.0", 0)
		else:
			self.SetStatusText("Y = " + str(y) + ", Data = " + str(self.image_y[y]), 0)
	#---------------------------------------------------------------------------
	def CTRL_UPKeyPressed(self, event):
		self.yValue += 1
		self.updatePlots()
	
	#---------------------------------------------------------------------------
	def CTRL_DOWNKeyPressed(self, event):
		self.yValue -= 1
		self.updatePlots()
	#---------------------------------------------------------------------------
	def CTRL_LEFTKeyPressed(self, event):
		self.xValue -= 1
		self.updatePlots()
	#---------------------------------------------------------------------------
	def CTRL_RIGHTKeyPressed(self, event):
		self.xValue += 1
		self.updatePlots()
	#---------------------------------------------------------------------------
	def menu_colormap_selection(self,event):
		self.staticText_colormap.SetLabel("Colormap: " + self.menubar.FindItemById(event.GetId()).GetText())
		
		self.imageColorMap = self.colormapList[self.menubar.FindItemById(event.GetId()).GetText()]
		
		self.axes4.imshow(self.colormap_display_object,aspect='auto',cmap=cm.get_cmap(self.imageColorMap),origin="lower")
		self.canvas4.draw()
		if self.image != None:
			self.axes1_drawplot()
	
	#---------------------------------------------------------------------------
	def treeCtrl_FITSinfo_LeftDClick(self, event):
		selection = self.treeCtrl_FITSinfo.GetSelection()
		text = self.treeCtrl_FITSinfo.GetItemText(self.treeCtrl_FITSinfo.GetFirstChild(selection)[0])
		if self.OS == "Windows":
			t, drive, path = text.split(':')
			drive = drive.lstrip()
			FITSfile = drive + ":" + path + "\\" + self.treeCtrl_FITSinfo.GetItemText(selection)
		elif self.OS == "Linux":
			t, path = text.split(':')
			FITSfile = path + '/' + self.treeCtrl_FITSinfo.GetItemText(selection)
		
		self.displayFITSImage(FITSfile)
		
	def treeCtrl_FITSinfo_RightClick(self,event):
		# record what was selected
		#print event.GetSelection(), self.treeCtrl_FITSinfo.GetItemText(self.treeCtrl_FITSinfo.GetSelection())
		self.tree_item_clicked = self.treeCtrl_FITSinfo.GetSelection()
		
		# Creates wxMenu
		self.tree_contextMenu = wx.Menu()
		for (id,title) in self.tree_menu_title_by_id.items():
			self.tree_contextMenu.Append(id, title)
			wx.EVT_MENU(self.tree_contextMenu, id, self.contextMenuSelected)
			
		#Displays menu with call to PopupMenu, invoked on the source component, passing event's GetPoint
		self.treeCtrl_FITSinfo.PopupMenu(self.tree_contextMenu, event.GetPoint())
		self.tree_contextMenu.Destroy() # destroy to avoid mem leak
		
	def contextMenuSelected(self, event):
		operation = self.tree_menu_title_by_id[event.GetId()]
		
		if operation == "Open":
			selection = self.treeCtrl_FITSinfo.GetSelection()
			text = self.treeCtrl_FITSinfo.GetItemText(self.treeCtrl_FITSinfo.GetFirstChild(selection)[0])
			t, drive, path = text.split(':')
			drive = drive.lstrip()
			FITSfile = drive + ":" + path + "\\" + self.treeCtrl_FITSinfo.GetItemText(selection)
			self.displayFITSImage(FITSfile)
		elif operation == "Delete":
			self.treeCtrl_FITSinfo.Delete(self.treeCtrl_FITSinfo.GetSelection())
	
	#---------------------------------------------------------------------------
	def On_About_Select(self, event):
		dlg = AboutBox()
		dlg.ShowModal()
		dlg.Destroy()
	
	#---------------------------------------------------------------------------
	def onQuit(self,event):
#		dlg = wx.MessageDialog(self,message = "Are you sure you want to Quit?",caption = "Quit?",style = wx.YES_NO|wx.NO_DEFAULT|wx.ICON_QUESTION)
#		choice = dlg.ShowModal()
#		dlg.Destroy()
#		
#		if choice == wx.ID_YES:
			#self.Destroy()  # self.Destroy() Not working
			wx.Exit()

###############################################################################
if __name__ == "__main__":
	app = wx.App()
	window = FITSFileViewer(None)
	window.Show(True)
	app.MainLoop()
